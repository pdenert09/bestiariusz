package pl.zhr.hak.bestiariusz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView textViewTitle;
    EditText editTextOwner;
    Button buttonCreate;

    SharedPreferences sharedPreferences;
    public static final String sharedPreferencesName = "data";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = getSharedPreferences(sharedPreferencesName, 0);
        String owner = sharedPreferences.getString("owner", "");
        if (!owner.equals("")) {
            //Przygotowujemy przejście do nowej aktywności
            Intent homeActivity = new Intent(MainActivity.this, HomeActivity.class);
            // uruchamiamy nową aktywność
            startActivity(homeActivity);
            // Kończymy starą aktywność - jeśli tego nie zrobimy, to dalej będzie działać w tle
            finish();
        }

        textViewTitle = findViewById(R.id.textViewTitle);
        editTextOwner = findViewById(R.id.editTextOwner);
        buttonCreate = findViewById(R.id.buttonCreate);

        buttonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String owner = editTextOwner.getText().toString();
                if (owner.isEmpty()) {
                    Toast.makeText(MainActivity.this,
                            getString(R.string.empty_owner_field),
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this,
                            getString(R.string.owner_with_name, owner),
                            Toast.LENGTH_SHORT).show();

                    //zapisywanie danych z aplikacji
                    sharedPreferences.edit().putString("owner", editTextOwner.getText().toString()).apply();

                    //Przygotowujemy przejście do nowej aktywności
                    Intent homeActivity = new Intent(MainActivity.this, HomeActivity.class);
                    // Przekazujemy do nowej aktywności wartość zmiennej owner
                    homeActivity.putExtra("owner", owner);
                    // uruchamiamy nową aktywność
                    startActivity(homeActivity);
                    // Kończymy starą aktywność - jeśli tego nie zrobimy, to dalej będzie działać w tle
                    finish();
                }
            }
        });
    }
}
