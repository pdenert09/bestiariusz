package pl.zhr.hak.bestiariusz;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BeastViewModel extends AndroidViewModel {

    private ExecutorService executorService;
    private BeastDao beastDao;

    public BeastViewModel(@NonNull Application application) {
        super(application);
        beastDao = AppDatabase.getInstance(application).beastDao();
        executorService = Executors.newSingleThreadExecutor();
    }

    public LiveData<List<Beast>> getBeastList() {
        return beastDao.getAll();
    }

    public void insert(Beast beast) {
        executorService.execute(() -> beastDao.insert(beast));
    }

    public void update(Beast beast) {
        executorService.execute(() -> beastDao.update(beast));
    }

    public void delete(Beast beast) {
        executorService.execute(() -> beastDao.delete(beast));
    }
}
