package pl.zhr.hak.bestiariusz;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(
        entities = {
                Beast.class
        },
        version = 1
)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public static AppDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(
                    context.getApplicationContext(),
                    AppDatabase.class,
                    "Bestiary"
            ).allowMainThreadQueries().build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() { INSTANCE = null; }

    public abstract BeastDao beastDao();
}

