package pl.zhr.hak.bestiariusz;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface BeastDao {
    @Query("SELECT * FROM Beasts")
    LiveData<List<Beast>> getAll();

    @Query("SELECT * FROM Beasts WHERE id = :id")
    LiveData<Beast> getItemById(int id);

    @Query("SELECT * FROM Beasts WHERE name LIKE :phrase")
    LiveData<List<Beast>> getByName(String phrase);

    @Insert
    void insert(Beast beast);

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Beast> beasts);

    @Delete
    void delete(Beast beast);

    @Update
    void update(Beast beast);

    @Query("DELETE FROM Beasts")
    void deleteAll();
}
