package pl.zhr.hak.bestiariusz;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import static pl.zhr.hak.bestiariusz.MainActivity.sharedPreferencesName;

public class HomeActivity extends AppCompatActivity {

    TextView textView;
    FloatingActionButton floatingActionButton;

    SharedPreferences sharedPreferences;
    String owner = "";

    List<Beast> beastList = new ArrayList<>();
    BeastAdapter beastAdapter;
    int REQUEST_CODE = 1;

    BeastViewModel beastViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        sharedPreferences = getSharedPreferences(sharedPreferencesName, 0);

        // Pobieranie przekazanej wartości
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            owner = extras.getString("owner", "");
        }
        if (owner.equals("")) {
            owner = sharedPreferences.getString("owner", "");
        }

        // ustawienie pobranej wiadomości jako tekst w aplikacji
        textView = findViewById(R.id.textViewOwner);
        textView.setText(owner);

        floatingActionButton = findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addBeastIntent = new Intent(HomeActivity.this, AddBeastActivity.class);
                startActivityForResult(addBeastIntent, REQUEST_CODE);
            }
        });

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        beastAdapter = new BeastAdapter(beastList, HomeActivity.this);
        recyclerView.setAdapter(beastAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        beastViewModel = new ViewModelProvider(this).get(BeastViewModel.class);
        beastViewModel.getBeastList().observe(this, new Observer<List<Beast>>() {
            @Override
            public void onChanged(List<Beast> beasts) {
                beastList = beasts;
                beastAdapter.setBeasts(beastList);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String beastName = data.getStringExtra("name");
                int beastHealth = data.getIntExtra("health", 1000);
//                 Nowy sposób dodawania do listy
                beastViewModel.insert(new Beast(beastName, beastHealth));
//                 Stary sposób dodawania do listy
//                beastList.add(new Beast(beastName, beastHealth));
//                beastAdapter.setBeasts(beastList);
            }
        }
    }

}
