package pl.zhr.hak.bestiariusz;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class BeastAdapter extends RecyclerView.Adapter<BeastAdapter.BeastViewHolder> {
    class BeastViewHolder extends  RecyclerView.ViewHolder {
        private final TextView name;
        private final TextView health;
        public BeastViewHolder(@NonNull View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.textViewName);
            this.health = itemView.findViewById(R.id.textViewHealth);
        }
    }

    private List<Beast> mBeastList;
    private final LayoutInflater mInflater;
    private Context mContext;

    public BeastAdapter(List<Beast> beastList, Context context) {
        this.mBeastList = beastList;
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public BeastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.listitem_beast, parent, false);
        return new BeastViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BeastViewHolder holder, int position) {
        holder.name.setText(mBeastList.get(position).getName());
        holder.health.setText(String.valueOf(mBeastList.get(position).getHealth()));
    }

    @Override
    public int getItemCount() {
        return mBeastList.size();
    }

    void setBeasts(List<Beast> beasts) {
        mBeastList = beasts;
        // mówi Adapterowi, że dane się zmieniły, więc musi odświeżyć całą listę
        notifyDataSetChanged();
    }
}
