package pl.zhr.hak.bestiariusz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static pl.zhr.hak.bestiariusz.MainActivity.sharedPreferencesName;

public class AddBeastActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    int counter;

    EditText editTextName;
    EditText editTextNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_beast);

        sharedPreferences = getSharedPreferences(sharedPreferencesName, 0);
        counter = sharedPreferences.getInt("counter", 0);

        if (counter == 0) {
            Toast.makeText(AddBeastActivity.this, getString(R.string.welcome), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(AddBeastActivity.this, getString(R.string.welcome_n, counter), Toast.LENGTH_SHORT).show();
        }
        editTextName = findViewById(R.id.editTextName);
        editTextNumber = findViewById(R.id.editTextBeastHealth);
        final Button addBestButton = findViewById(R.id.buttonAddBeast);
        addBestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String beastName = editTextName.getText().toString();
                String beastHealth = editTextNumber.getText().toString();
                if (!beastName.equals("") && !beastHealth.equals("")) {
                    addBeast(beastName, Integer.parseInt(beastHealth));
                } else {
                    Toast.makeText(AddBeastActivity.this, getString(R.string.empty_fields), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        sharedPreferences.edit().putInt("counter", counter+1).apply();
    }

    void addBeast(String name, int health) {
        Intent mIntent = new Intent();
        mIntent.putExtra("name", name);
        mIntent.putExtra("health", health);
        setResult(RESULT_OK, mIntent);
        finish();
    }
}
